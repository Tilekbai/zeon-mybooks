from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)

from read_books.models import ReadBook


class LockedView(LoginRequiredMixin):
    login_url = "admin:login"

class ReadBooksList(LockedView, ListView):
    model = ReadBook
    queryset = ReadBook.objects.all().order_by("-created_at")

class ReadBookDetail(LockedView, DetailView):
    model = ReadBook

class ReadBookCreate(LockedView, SuccessMessageMixin, CreateView):
    model = ReadBook
    fields = ["title", "author", "description"]
    success_message = "New book was created!"
    success_url = reverse_lazy("read_books_list")

class ReadBookUpdate(LockedView, SuccessMessageMixin, UpdateView):
    model = ReadBook
    fields = ["title", "author", "description"]
    success_message = "Book was updated!"

    def get_success_url(self):
        return reverse_lazy(
            "read_books_detail",
            kwargs={"pk": self.object.pk}
        )

class ReadBookDelete(LockedView, SuccessMessageMixin, DeleteView):
    model = ReadBook
    success_url = reverse_lazy("read_books_list")
    success_message = "Your book was deleted!"

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, self.success_message)
        return super().delete(request, *args, **kwargs)


