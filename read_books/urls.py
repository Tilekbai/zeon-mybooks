from django.urls import path
from read_books import views


urlpatterns = [
    path("", views.ReadBooksList.as_view(), name="read_books_list"),
    path("book/<int:pk>", views.ReadBookDetail.as_view(), name="read_books_detail"),
    path("book/<int:pk>/update", views.ReadBookUpdate.as_view(), name="read_books_update"),
    path("book/create", views.ReadBookCreate.as_view(), name="read_books_create"),
    path("book/<int:pk>/delete", views.ReadBookDelete.as_view(), name="read_books_delete"),
]