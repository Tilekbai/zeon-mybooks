from django.db import models


class ReadBook(models.Model):
    title = models.CharField(max_length=70, null=False)
    author = models.CharField(max_length=70, null=False)
    description = models.TextField()
    created_at = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Read books"

